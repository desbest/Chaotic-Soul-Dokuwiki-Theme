<?php
/**
 * DokuWiki Chaotic Soul Template
 * Based on the starter template and a wordpress theme of the same name
 *
 * @link     http://dokuwiki.org/template:chaoticsoul
 * @author   desbest <afaninthehouse@gmail.com>
 * @license  GPL 2 (http://www.gnu.org/licenses/gpl.html)
 */

if (!defined('DOKU_INC')) die(); /* must be run from within DokuWiki */
@require_once(dirname(__FILE__).'/tpl_functions.php'); /* include hook for template functions */

$showTools = !tpl_getConf('hideTools') || ( tpl_getConf('hideTools') && !empty($_SERVER['REMOTE_USER']) );
$showSidebar = page_findnearest($conf['sidebar']) && ($ACT=='show');
$sidebarElement = tpl_getConf('sidebarIsNav') ? 'nav' : 'aside';
?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $conf['lang'] ?>"
  lang="<?php echo $conf['lang'] ?>" dir="<?php echo $lang['direction'] ?>" class="no-js">
<head>
    <meta charset="UTF-8" />
    <title><?php tpl_pagetitle() ?> [<?php echo strip_tags($conf['title']) ?>]</title>
    <script>(function(H){H.className=H.className.replace(/\bno-js\b/,'js')})(document.documentElement)</script>
    <?php tpl_metaheaders() ?>
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <?php echo tpl_favicon(array('favicon', 'mobile')) ?>
    <?php tpl_includeFile('meta.html') ?>
</head>

<body id="dokuwiki__top" class="site <?php echo tpl_classes(); ?> <?php echo ($showSidebar) ? 'hasSidebar' : ''; ?>">

<div id="page">


<div id="header">
    <?php tpl_includeFile('header.html') ?>
     <h1><?php tpl_link(wl(),$conf['title'],'accesskey="h" title="[H]"') ?></h1>
        <?php /* how to insert logo instead (if no CSS image replacement technique is used):
                upload your logo into the data/media folder (root of the media manager) and replace 'logo.png' accordingly:
                tpl_link(wl(),'<img src="'.ml('logo.png').'" alt="'.$conf['title'].'" />','id="dokuwiki__top" accesskey="h" title="[H]"') */ ?>
    <?php if ($conf['tagline']): ?>
        <div class="description"><?php echo $conf['tagline'] ?></div>
    <?php endif ?>
    <ul class="a11y skip">
        <li><a href="#dokuwiki__content"><?php echo $lang['skip_to_content'] ?></a></li>
    </ul>
    <div class="clearer"></div>
</div>

<div class="hr">&nbsp;</div>

<div id="headerimg" class="clearfix">
    <div id="header-overlay"> </div>
    
    <div id="header-image"><img alt="header image" src="<?php echo tpl_basedir();?>/images/headerimage.jpg"></div>
</div>
<?php html_msgarea() /* occasional error and info messages on top of the page */ ?>
  <!-- BREADCRUMBS -->
<?php if($conf['breadcrumbs']){ ?>
    <div class="breadcrumbs"><?php tpl_breadcrumbs() ?></div>
<?php } ?>
<?php if($conf['youarehere']){ ?>
<?php } ?>
<div class="hr">&nbsp;</div>
<!-- </div> -->

<div id="wrapper" class="clearfix">

    
    
    <div id="content" class="widecolumn">
    
            
        <div class="post">
            <!-- <h2 class="title"><a href="http://localhost/wordpress/test-post/" rel="bookmark" title="Permanent Link to Test post">Test post</a></h2> -->

            <!-- desbest edit -->
            <!-- <img src="http://localhost/wordpress/wp-content/uploads/2020/02/anime-50.jpg" class="postthumbnail"> -->
                            
            <div class="entrytext">
                <!-- ********** CONTENT ********** -->
                <main id="dokuwiki__content"><!-- <div class="pad"> -->
                    <?php tpl_flush() /* flush the output buffer */ ?>
                    <?php tpl_includeFile('pageheader.html') ?>

                    <div class="page">
                        <!-- wikipage start -->
                        <?php tpl_content() /* the main content */ ?>
                        <!-- wikipage stop -->
                        <div class="clearer"></div>
                    </div>

                    <?php tpl_flush() ?>
                    <?php tpl_includeFile('pagefooter.html') ?>
                <!-- </div> --></main><!-- /content -->

                <!-- <p class="authormeta">~ by desbest on ?// the_date(); ?.</p>
                <a href="http://localhost/wordpress/category/uncategorized/" rel="category tag">Uncategorized</a> -->                           
            </div>
        </div>
        

    
        
    </div>
    
    <div id="sidebar">

            <?php if ($showSidebar): ?>
                <<?php echo $sidebarElement ?> id="writtensidebar" aria-label="<?php echo $lang['sidebar'] ?>"><div class="pad aside include group">
                    <?php tpl_includeFile('sidebarheader.html') ?>
                    <?php tpl_include_page($conf['sidebar'], 1, 1) /* includes the nearest sidebar page */ ?>
                    <?php tpl_includeFile('sidebarfooter.html') ?>
                    <div class="clearer"></div>
                </div></<?php echo $sidebarElement ?>><!-- /aside -->
            <?php endif; ?>

            <?php tpl_searchform() ?>
            
            <!-- SITE TOOLS -->
                <h3>Site Tools</h3>
                <nav id="dokuwiki__sitetools" aria-labelledby="dokuwiki__sitetools_heading">
                    <h3 class="a11y" id="dokuwiki__sitetools_heading"><?php echo $lang['site_tools'] ?></h3>
                    
                    <?php
                        // mobile menu (combines all menus in one dropdown)
                        // if (file_exists(DOKU_INC . 'inc/Menu/MobileMenu.php')) {
                        //     echo (new \dokuwiki\Menu\MobileMenu())->getDropdown($lang['tools']);
                        // } else {
                        //   tpl_actiondropdown($lang['tools']);
                        // }
                    ?>
                    <ul>
                        <?php if (file_exists(DOKU_INC . 'inc/Menu/SiteMenu.php')) {
                            echo (new \dokuwiki\Menu\SiteMenu())->getListItems('action ', false);
                        } else {
                            _tpl_sitetools();
                        } ?>
                    </ul>
                </nav>

                 <!-- PAGE ACTIONS -->
            <?php if ($showTools): ?>
                <h3>Page Tools</h3>
                <nav id="" aria-labelledby="dokuwiki__pagetools_heading">
                    <h3 class="a11y" id="dokuwiki__pagetools_heading"><?php echo $lang['page_tools'] ?></h3>
                    <ul>
                        <?php if (file_exists(DOKU_INC . 'inc/Menu/PageMenu.php')) {
                            echo (new \dokuwiki\Menu\PageMenu())->getListItems('action ', false);
                        } else {
                            _tpl_pagetools();
                        } ?>
                    </ul>
                </nav>
            <?php endif; ?>

             <!-- USER TOOLS -->
                <?php if ($conf['useacl'] && $showTools): ?>
                    <h3>User Tools</h3>
                    <nav id="" aria-labelledby="dokuwiki__usertools_heading">
                        <h3 class="a11y" id="dokuwiki__usertools_heading"><?php echo $lang['user_tools'] ?></h3>
                        <ul>
                            <?php if (!empty($_SERVER['REMOTE_USER'])) {
                                echo '<li class="user">';
                                tpl_userinfo(); /* 'Logged in as ...' */
                                echo '</li>';
                            } ?>
                            <?php if (file_exists(DOKU_INC . 'inc/Menu/UserMenu.php')) {
                                /* the first parameter is for an additional class, the second for if SVGs should be added */
                                echo (new \dokuwiki\Menu\UserMenu())->getListItems('action ', false);
                            } else {
                                /* tool menu before Greebo */
                                _tpl_usertools();
                            } ?>
                        </ul>
                    </nav>
                <?php endif ?>
</div>



</div>

<div class="hr">&nbsp;</div>
<div id="footer">
    <p>Powered by <a href="http://wordpress.org/">WordPress</a>. Theme: ChaoticSoul (v1.4) by <a href="http://avalonstar.com" rel="designer">Bryan Veloso</a> and <a href="http://desbest.com">desbest</a>.</p>
    <div class="doc"><?php tpl_pageinfo() /* 'Last modified' etc */ ?></div>
    <?php tpl_license('button') /* content license, parameters: img=*badge|button|0, imgonly=*0|1, return=*0|1 */ ?>

    <?php tpl_includeFile('footer.html') ?>

    <div class="no"><?php tpl_indexerWebBug() /* provide DokuWiki housekeeping, required in all templates */ ?></div>
</div>
    <?php /* the "dokuwiki__top" id is needed somewhere at the top, because that's where the "back to top" button/link links to */ ?>
    <?php /* tpl_classes() provides useful CSS classes; if you choose not to use it, the 'dokuwiki' class at least
             should always be in one of the surrounding elements (e.g. plugins and templates depend on it) */ ?>

</body>
</html>
