# Chaotic Soul dokuwiki template

* Based on a wordpress theme
* Designed by [Avalon Star](https://github.com/avalonstar/chaoticsoul)
* Converted by [desbest](http://desbest.com)
* Metadata is in template.info.txt
* Under the GPL license (see copying file)
* [More information](http://dokuwiki.org/template:chaoticsoul)

![chaotic soul theme screenshot](https://i.imgur.com/kJvfM9l.png)